import { Component, OnInit} from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  title = 'front';

  constructor (private socket: Socket) {}

  ngOnInit(): void {
    this.socket.fromEvent('message').pipe(map((data: any) => data.msg));
  }
}
